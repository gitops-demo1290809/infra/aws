resource "aws_s3_bucket" "example-bucket" {
  bucket = "cny-terraform-example"
  acl    = "private"
  tags = {
    Name        = "cny-terraform-example"
    Environment = "dev"
  }
}

# Create a s3 bucket with images
resource "aws_s3_bucket" "images" {
  bucket = "cny-terraform-images"
  acl    = "private"
  tags = {
    Name        = "cny-terraform-images"
    Environment = "dev"
  }
}