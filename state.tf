terraform {
  backend "s3" {
    bucket = "cny-terraform-state"
    key    = "eu-north-1/eks/terraform.tfstate"
    region = "eu-north-1"
  }
}

